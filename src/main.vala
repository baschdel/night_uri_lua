
public static int luaopen_night_uri(Lua.LuaVM luavm){
	luavm.new_table();
	int top = luavm.get_top();
	table_set_function(luavm, top,"parse",parse_uri);
	table_set_function(luavm, top,"generate",generate_uri);
	table_set_function(luavm, top,"join",join_uri);
	return 1;
}

public static int join_uri(Lua.LuaVM luavm){
	if (luavm.is_none_or_nil(1)) {
		return 0;
	}
	if (luavm.is_none_or_nil(2)) {
		if (luavm.is_nil(2)){
			luavm.pop(1);
		}
		return 1;
	}
	string joined = Night.Util.Uri.join(luavm.to_string(1), luavm.to_string(2));
	luavm.pop(2);
	luavm.push_string(joined);
	return 1;
}

public static int parse_uri(Lua.LuaVM luavm){
	if (luavm.is_none_or_nil(1)) {
		return 0;
	}
	string uri = luavm.to_string(1);
	luavm.pop(1);
	var parsed_uri = new Night.Util.ParsedUri(uri);
	luavm.new_table();
	int top = luavm.get_top();
	table_set_string(luavm, top, "scheme", parsed_uri.scheme);
	table_set_string(luavm, top, "authority", parsed_uri.authority);
	table_set_string(luavm, top, "index", parsed_uri.index);
	table_set_string(luavm, top, "query", parsed_uri.query);
	table_set_string(luavm, top, "path", parsed_uri.path);
	table_set_string(luavm, top, "username", parsed_uri.username);
	table_set_string(luavm, top, "host", parsed_uri.host);
	table_set_string(luavm, top, "port", parsed_uri.port);
	return 1;
}

public static int generate_uri(Lua.LuaVM luavm){
	if (!luavm.is_table(1)) {
		return 0;
	}
	var uri = new Night.Util.ParsedUri.blank();
	uri.scheme = table_get_string(luavm, 1, "scheme");
	uri.index = table_get_string(luavm, 1, "index");
	uri.query = table_get_string(luavm, 1, "query");
	uri.path = table_get_string(luavm, 1, "path");
	string? authority = table_get_string(luavm, 1, "authority");
	if (authority == null) {
		uri.username = table_get_string(luavm, 1, "username");
		uri.host = table_get_string(luavm, 1, "host");
		uri.port = table_get_string(luavm, 1, "port");
	} else {
		uri.authority = authority;
	}
	luavm.push_string(uri.uri);
	return 1;
}

private static void table_set_string(Lua.LuaVM luavm, int table_index, string key, string? val){
	if (val == null) { return; }
	luavm.push_string(key);
	luavm.push_string(val);
	luavm.set_table(table_index);
}

private static string? table_get_string(Lua.LuaVM luavm, int table_index, string key){
	luavm.push_string(key);
	luavm.get_table(table_index);
	if (luavm.is_none_or_nil(-1)) {
		return null;
	}
	string result = luavm.to_string(-1);
	luavm.pop(1);
	return result;
}

private static void table_set_function(Lua.LuaVM luavm, int table_index, string key, Lua.CallbackFunc function){
	luavm.push_string(key);
	luavm.push_cfunction(function);
	luavm.set_table(table_index);
}
