public class Night.Util.Stack<G> : Object{
	
	public List<G> list = new List<G>();
	
	public void push(G thingy){
		lock(list){
			list.append(thingy);
		}
	}
	
	public void fifo_push(G thingy){
		lock(list){
			list.prepend(thingy);
		}
	}
	
	public G? pop(){
		lock(list){
			unowned List<G> entry = list.last();
			if(entry == null){ return null; }
			list.remove_link(entry);
			return entry.data;
		}
	}
	
	public void clear(){
		lock(list){
			while (list.length()>0){
				this.pop();
			}
		}
	}
	
	public uint size(){
		return list.length();
	}
	
}
