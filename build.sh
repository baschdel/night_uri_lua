#!/bin/sh
builddir=meson_build
output="src/libnight_uri.so"
dest="night_uri.so"

if [ ! -d "$builddir" ]
then
	meson "$builddir" --prefix=/usr
fi

cd "$builddir"
rm -f "$output"
ninja
cp -f "$output" "../$dest"
