night_uri = require("night_uri")

function test_assert(name,value,expected)
	if value[name] == expected[name] then
		print("\t       "..name.."\t '"..tostring(value[name]).."' == '"..tostring(expected[name]).."'")
		return true
	else
		print("\t[FAIL] "..name.."\t '"..tostring(value[name]).."' == '"..tostring(expected[name]).."'")
		return false
	end
end

function test_parser(uri,expected)
	print("Testing: "..uri)
	local result = night_uri.parse(uri)
	local passed = true
	test_assert("scheme",result,expected)
	test_assert("authority",result,expected)
	test_assert("username",result,expected)
	test_assert("host",result,expected)
	test_assert("path",result,expected)
	test_assert("port",result,expected)
	test_assert("index",result,expected)
	test_assert("query",result,expected)
	return passed
end

test_parser("//example.org:blub/test",{
	host = "example.org",
	port = "blub",
	authority = "example.org:blub",
	path = "/test",
})

test_parser("https://someone@example.org:8080/test/foo.txt?something#somewhere",{
	scheme = "https",
	authority = "example.org:8080",
	host = "example.org",
	port = "8080",
	username = "someone",
	path = "/test/foo.txt",
	query = "something",
	index = "somewhere",
})

test_parser("/foo:1.txt?#",{
	path = "/foo:1.txt",
	query = "",
	index = "",
})

test_parser("https://www.example.org/?redirect_to=https://www.example.com",{
	scheme = "https",
	authority = "www.example.org",
	host = "www.example.org",
	path = "/",
	query = "redirect_to=https://www.example.com",
})

test_parser("https://ocdoc.cil.li/component:modem",{
	scheme = "https",
	authority = "ocdoc.cil.li",
	host = "ocdoc.cil.li",
	path = "/component:modem",
})
