# Night Uri Lua

Lua bindings for the NightCat uri parser.

## How to build

Dependencies:
* lua 5.3
* vala
* meson

Run the build.sh file and the script will setup the build envoirenment will put a night_uri.so file in the projects root directory.
This file can then be placed somewhere, where lua searches for modules

## How to use

```
local uri = require("night_uri")

-- parsing uris

local parsed_uri = uri.parse("https://someone@example.org:8080/test/foo.txt?something#somewhere")

print(parsed_uri.scheme) -- https
print(parsed_uri.authority) -- example.org:8080
print(parsed_uri.username) -- someone
print(parsed_uri.host) -- example.org
print(parsed_uri.path) -- /test/foo.txt
print(parsed_uri.port) -- 8080
print(parsed_uri.index) -- somewhere
print(parsed_uri.query) -- something

--generating uris

--if the authority field is present, it will be used instead of the other components
parsed_uri.authority = nil
parsed_uri.index = nil -- remove the index
parsed_uri.host = "example-mirror.org"
parsed_uri.port = "7070"
parsed_uri.scheme = "gopher"
parsed_uri.path = "0"..parsed_uri.path
parsed_uri.username = nil

local gopher_uri = uri.generate(parse_uri)
print(gopher_uri) -- gopher://example-mirror.org:7070/0/test/foo.txt?something

-- joining uris

local other_uri = uri.join(gopher_uri,"../bar.txt?something-else#foo")
print(other_uri) -- gopher://example-mirror.org:7070/0/bar.txt?something-else#foo

```

## Development scripts

run: when executed will run the build.sh script and start a lua shell with night_uri imported as uri

update_src_build_files: will put all files in the src/ folder with a filename ending in .vala into the src/meson.build file

The upstream for src/utils is the NightCat browser project (wich at the time of writing 2020-11 is nowhere near finished) 

